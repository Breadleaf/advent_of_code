#include "cpp_py.hpp"
#include "event_funcs.hpp"
namespace py = cpp_py;
using dt = int;

int main()
{
	vector<vector<dt>> v = ef::group_parse<dt>();

	vector<dt> elf;
	for (vector<dt> vd: v) elf.push_back(py::sum(vd));

	py::print(py::max(elf));
	py::sort(elf);
	py::print(py::sum(vector<dt>(elf.begin() + elf.size() - 3, elf.end())));

	return 0;
}
