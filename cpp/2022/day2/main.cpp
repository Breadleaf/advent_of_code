#include "event_funcs.hpp"
namespace py = cpp_py;
using dt = int;
using dp = pair<char, char>;

int main()
{
	vector<dp> v = ef::pair_uniform_parse<char, char>();
	for (dp p: v)
	{
		py::print(get<0>(p), get<1>(p));
	}
	return 0;
}
