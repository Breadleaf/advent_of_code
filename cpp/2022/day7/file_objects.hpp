#ifndef FILE_OBJECTS_HPP
#define FILE_OBJECTS_HPP

#include "event_funcs.hpp"

enum fs_types
{
	file,
	dir
};

class ft_parent
{
public:
	string name;
	fs_types fs_type;
	virtual int get_size() const { return 0; }
	fs_types get_fs_type();
	virtual string get_name() const { return "Base Class"; }
	virtual void print() const { cout << "(Base) " << this->get_name() << endl; }
};

class ft_file : public ft_parent
{
public:
	fs_types fs_type;
	string name;
	int size;

	ft_file(string nm, int sz) : name(nm), size(sz) { fs_type = fs_types::file; }
	virtual int get_size() const override { return size; }
	fs_types get_fs_type() { return fs_type; }
	virtual string get_name() const override { return name; }
	virtual void print() const override { cout << "(file) " << this->get_name() << endl; }
};

class ft_directory : public ft_parent
{
public:
	fs_types fs_type;
	string name;
	vector<ft_directory*> sub_dirs;
	vector<ft_file*> sub_files;
	vector<ft_parent*> sub_objs;

	// Constructors
	ft_directory() { fs_type = fs_types::dir; }
	ft_directory(string s) : name(s) { fs_type = fs_types::dir; }

	virtual string get_name() const override { return name; }

	virtual int get_size() const override
	{
		int total = 0;

		for (ft_parent* obj: sub_objs)
		{
			total += obj->get_size();
		}

		return total;
	}

	virtual void print() const override
	{
		cout << "(dir) " << this->name << endl;

		for (ft_parent* obj: this->sub_objs)
		{
			obj->print();
		}
	}
};

class file_system
{
private:
	ft_directory* root_dir;
	ft_directory* prev_dir;
	ft_directory* current_dir;
public:
	file_system()
	{
		ft_directory temp("/");
		root_dir = &temp;
		prev_dir = &temp;
		current_dir = &temp;
	}

	void add_dir(string dir_name)
	{
		ft_directory temp(dir_name);
		current_dir->sub_dirs.push_back(&temp);
		current_dir->sub_objs.push_back(&temp);
	}

	void add_file(string file_name, int file_size)
	{
		ft_file temp(file_name, file_size);
		current_dir->sub_files.push_back(&temp);
		current_dir->sub_objs.push_back(&temp);
	}

	void move_dir(string s)
	{
		if (s == "..")
		{
			current_dir = prev_dir;
			return;
		}

		for (ft_directory* dir: current_dir->sub_dirs)
		{
			if (dir->get_name() == s)
			{
				current_dir = dir;
			}
		}
	}

	string get_current_dir()
	{
		return current_dir->name;
	}
};

#endif
