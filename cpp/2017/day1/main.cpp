#include "event_funcs.hpp"
namespace py = cpp_py;

int main()
{
	ifstream f("./input");
	string s;
	f >> s;

	vector<int> vi;
	for (char c: s) vi.push_back(atoi(&c));

	int total = 0;

	for (size_t idx = 0; idx < vi.size() - 1; idx++)
	{
		if (vi[idx] == vi[idx + 1])
		{
			total += vi[idx];
		}
	}

	if (vi[vi.size() - 1] == vi[0]) total += vi[0];
	py::print(total);

	// Part 2

	total = 0;

	int hw = (vi.size() / 2) - 1;

	for (size_t idx = 0; idx < vi.size() - 1; idx++)
	{
		if (vi[idx] == vi[(hw + 1 + idx) % vi.size()])
		{
			total += vi[idx];
		}
	}

	if (vi[vi.size() - 1] == vi[hw]) total += vi[hw];
	py::print(total);

	return 0;
}
