#ifndef EVENT_FUNCS_HPP
#define EVENT_FUNCS_HPP

#include <bits/stdc++.h>
#include "cpp_py.hpp"

using namespace std;

namespace ef {

	template <class T> // ;
	vector<vector<T>> group_parse()
	{
		ifstream file("./input");

		vector<string> vs;
		while(!file.eof())
		{
			string s;
			getline(file, s);
			vs.push_back(s);
		}
		vs.pop_back();
	
		vector<vector<T>> vvt;
		vector<T> temp;
		for (string line: vs)
		{
			if (line != "")
			{
				T t;
				stringstream convert(line);
				convert >> t;
				temp.push_back(t);
				continue;
			}

			vvt.push_back(temp);
			temp.clear();
		}
		vvt.push_back(temp);

		return vvt;
	}

	template <class T> // ;
	vector<T> uniform_parse()
	{
		ifstream file("./input");

		vector<T> vt;
		while (!file.eof())
		{
			T t;
			file >> t;
			vt.push_back(t);
		}

		return vt;
	}

}

#endif // EVENT_FUNCS_HPP
