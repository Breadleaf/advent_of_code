#include "event_funcs.hpp"
namespace py = cpp_py;
using dt = int;

int main()
{
	vector<dt> v = ef::uniform_parse<dt>();
	
	int s_inc = 0;
	for (size_t i = 1; i < v.size(); i++) if (v[i] > v[i - 1]) s_inc += 1;
	py::print(s_inc);

	vector<int> depths;
	for (size_t i = 0; i < v.size() - 3; i++) depths.push_back(v[i] + v[i+1] + v[i+2]);

	s_inc = 0;
	for (size_t i = 1; i < depths.size(); i++) if (depths[i] > depths[i - 1]) s_inc += 1;
	py::print(s_inc);

	return 0;
}
