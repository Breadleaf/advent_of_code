#include "cpp_py.hpp"
#include "event_funcs.hpp"
namespace py = cpp_py;
using dt = int;
using dp = pair<string, int>;

int main()
{
	vector<dp> v = ef::pair_uniform_parse<string, int>();

	int vertical(0), horizontal(0);
	for (dp p: v)
	{
		switch (get<0>(p)[0])
		{
			case 'f':
				horizontal += get<1>(p);
				break;
			case 'u':
				vertical -= get<1>(p);
				break;
			case 'd':
				vertical += get<1>(p);
				break;
		}
	}
	py::print(vertical * horizontal);

	int aim(0);
	vertical = 0;
	horizontal = 0;
	for (dp p: v)
	{
		switch (get<0>(p)[0])
		{
			case 'f':
				horizontal += get<1>(p);
				vertical += aim * get<1>(p);
				break;
			case 'u':
				aim -= get<1>(p);
				break;
			case 'd':
				aim += get<1>(p);
				break;
		}
	}
	py::print(vertical * horizontal);

	return 0;
}
