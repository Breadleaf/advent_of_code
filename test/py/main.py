from event import *
t1 = int
t2 = int

a_tests = []
def part_a(data):
    return None

run_b = 0
b_tests = []
def part_b(data):
    return None

if __name__ == "__main__":
    # parse = lambda stream: [x.strip() for x in stream] # Line Breaks
    # parse = lambda stream: [x.strip() for x in stream if x.strip() != ""] # No Line Breaks
    # parse = lambda stream: [x.strip().split(" ") for x in stream] # Line Breaks Multiple Elements
    # parse = lambda stream: [x.strip().split(" ") for x in stream if x.strip() != ""] # No Line Breaks Multiple Elements

    # convert = lambda lst: [t1(x) for x in lst] # Single Elements
    # convert = lambda lst: [[t1(x[0]), t2(x[1])] for x in lst] # Multiple Elements
    # convert = lambda lst: [[t1(y) for y in x] for x in lst] # Split Line Multiple Line
    # convert = lambda lst: [[t1(y) for y in x] for x in lst][0] # Split Line Multiple Line

    if run_tests("A", part_a, parse, convert, a_tests):
        print(part_a(convert(parse(open("input")))))

    if not run_b:
        exit(0)

    print()

    if run_tests("B", part_b, parse, convert, b_tests):
        print(part_b(convert(parse(open("input")))))
