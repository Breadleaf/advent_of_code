#ifndef EVENT_FUNCS_HPP
#define EVENT_FUNCS_HPP

#include <bits/stdc++.h>
#include "cpp_py.hpp"

using namespace std;

namespace ef {

	vector<int> split_single_line_parse(istream &is)
	{
		string s;
		is >> s;

		vector<int> vi;
		for (char c: s) vi.push_back(atoi(&c));

		return vi;
	}

	template <class T, class H> // ;
	vector<pair<T, H>> pair_uniform_parse(istream &is)
	{
		vector<pair<T, H>> vth;
		while (!is.eof())
		{
			T t;
			H h;
			is >> t >> h;
			vth.push_back(pair<T, H>(t, h));
		}
		vth.pop_back();

		return vth;
	}

	template <class T> // ;
	vector<vector<T>> group_parse(istream &is)
	{
		vector<string> vs;
		while(!is.eof())
		{
			string s;
			getline(is, s);
			vs.push_back(s);
		}
		vs.pop_back();
	
		vector<vector<T>> vvt;
		vector<T> temp;
		for (string line: vs)
		{
			if (line != "")
			{
				T t;
				stringstream convert(line);
				convert >> t;
				temp.push_back(t);
				continue;
			}

			vvt.push_back(temp);
			temp.clear();
		}
		vvt.push_back(temp);

		return vvt;
	}

	template <class T> // ;
	vector<T> uniform_parse(istream &is)
	{
		vector<T> vt;
		while (!is.eof())
		{
			T t;
			is >> t;
			vt.push_back(t);
		}

		return vt;
	}

}

#endif // EVENT_FUNCS_HPP
