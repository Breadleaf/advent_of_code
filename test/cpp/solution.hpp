#ifndef SOLUTION_HPP
#define SOLUTION_HPP

#include "event_funcs.hpp"
namespace py = cpp_py;

using t1 = int;
using t2 = int;
using dp = pair<t1, t2>;

namespace test
{
	vector<pair<string, t2>> a = {
	}, b = {
	};
}

t2 part_a(istream &is)
{
	//vector<vector<t1>> v = ef::group_parse<t1>(); // Uniform Data x2 Elements Line Breaks
	//vector<t1> v = ef::uniform_parse<t1>(); // Uniform Data x1 Elements No Line Breaks
	//vector<dp> v = ef::pair_uniform_parse<t1, t2>(); // Non-Uniform Data x2 Elements No Line Breaks
	//vector<t1> v = ef::split_single_line_parse(); // Single Line Integer
	
	return 0;
}

t2 part_b(istream &is)
{
	//vector<vector<t1>> v = ef::group_parse<t1>(); // Uniform Data x2 Elements Line Breaks
	//vector<t1> v = ef::uniform_parse<t1>(); // Uniform Data x1 Elements No Line Breaks
	//vector<dp> v = ef::pair_uniform_parse<t1, t2>(); // Non-Uniform Data x2 Elements No Line Breaks
	//vector<t1> v = ef::split_single_line_parse(); // Single Line Integer
	
	return 0;
}

#endif // SOLUTION_HPP
