from event import *
t1 = int
t2 = str
t3 = float

# data = [x.strip() for x in open("./input")] # Line Breaks Uniform Data (Full Line)
data = [x.strip() for x in open("./input") if x.strip() != ""] # No Line Breaks Uniform Data (Full Line)
# data = [x.strip().split(" ") for x in open("./input")] # Line Breaks Non Uniform Data
# data = [x.strip().split(" ") for x in open("./input") if x.strip() != ""] # No Line Breaks Non Uniform Data

data = [t1(x) for x in data]
# data = [[t2(x[0]), t1(x[1])] for x in data]

# Part 1
num_increase = 0
for idx in range(len(data)):
    if data[idx] > data[idx - 1]:
        num_increase += 1
print(num_increase)

# Part 2
num_increase = 0
depths = [sum(data[x:x+3]) for x in range(len(data) - 2)]
[num_increase := num_increase + 1 for x in range(len(depths)) if depths[x] > depths[x - 1]]
print(num_increase)
