iport string

file = open("./input")

############ Part One ############

# Ugly duckling
thestuff = [int(sublist[0] + sublist[-1]) for sublist in
        [[char for char in line if char.isdigit()] for line in file]]

print(sum(thestuff))

############ Part Two ############

con = conversions = {
        "one"   : "1",
        "two"   : "2",
        "three" : "3",
        "four"  : "4",
        "five"  : "5",
        "six"   : "6",
        "seven" : "7",
        "eight" : "8",
        "nine"  : "9",
        "1"     : "1",
        "2"     : "2",
        "3"     : "3",
        "4"     : "4",
        "5"     : "5",
        "6"     : "6",
        "7"     : "7",
        "8"     : "8",
        "9"     : "9"
}

rkc = reverse_key_conversions = {k[::-1] : v for k, v in conversions.items()}

parsed_lines = []

# find first substring
file.seek(0) # reset file pointer
for line in file:
        done = False
        for buff_len in range(len(line)):
                for key in con.keys():
                        if key in line[:buff_len]:
                                parsed_lines.append(con[key])
                                done = True
                                break
                if done: break

# find last substring
file.seek(0) # reset file pointer
for idx, line in enumerate(file):
        line = line[::-1]
        done = False
        for buff_len in range(len(line)):
                for key in rkc.keys():
                        if key in line[:buff_len]:
                                parsed_lines[idx] += rkc[key]
                                done = True
                                break
                if done: break

# account for lines that only have 1 number in them
parsed_lines = [x * 2 if len(x) == 1 else x for x in parsed_lines]

print(sum([int(x) for x in parsed_lines]))
