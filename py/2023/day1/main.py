import string

def partOne():
    with open("./input") as f:
        all = []

        for line in f:
            number: str = ""

            for letter in line:

                if letter in string.digits:
                    number = number + letter

            all.append(int(number[0] + number[-1]))

        print(sum(all))

def parseline(line):
    cpy_line = line

    numbers = {
            "one":1,
            "two":2,
            "three":3,
            "four":4,
            "five":5,
            "six":6,
            "seven":7,
            "eight":8,
            "nine":9
    }

    inv_numbers = {v: k for k, v in numbers.items()}

    lpositions = {
            1:None,
            2:None,
            3:None,
            4:None,
            5:None,
            6:None,
            7:None,
            8:None,
            9:None
    }

    rpositions = {
            1:None,
            2:None,
            3:None,
            4:None,
            5:None,
            6:None,
            7:None,
            8:None,
            9:None
    }

    # search all the words
    for key in list(numbers.keys()):
        wline = cpy_line
        while (pos := wline.find(key)) != -1:
            lpositions.update({numbers[key]:pos}) if lpositions[numbers[key]] == None else None
            lpositions.update({numbers[key]:pos}) if pos < lpositions[numbers[key]] else None

            rpositions.update({numbers[key]:pos}) if rpositions[numbers[key]] == None else None
            rpositions.update({numbers[key]:pos}) if pos > rpositions[numbers[key]] else None

            wline = wline.replace(key, "x" * len(key), 1)

    # search all the numbers
    for key in [str(x) for x in list(numbers.values())]:
        wline = line
        lookup_key = inv_numbers[int(key)]
        while (pos := wline.find(key)) != -1:
            lpositions.update({numbers[lookup_key]:pos}) if lpositions[numbers[lookup_key]] == None else None
            lpositions.update({numbers[lookup_key]:pos}) if pos < lpositions[numbers[lookup_key]] else None

            rpositions.update({numbers[lookup_key]:pos}) if rpositions[numbers[lookup_key]] == None else None
            rpositions.update({numbers[lookup_key]:pos}) if pos > rpositions[numbers[lookup_key]] else None

            wline = wline.replace(key, "x" * len(key), 1)

    # clean lpositions
    cleaned_lpositions = dict([i for i in lpositions.items() if i[1] != None])

    # clean rpositions
    cleaned_rpositions = dict([i for i in rpositions.items() if i[1] != None])

    # sort the left positions by which has the smallest index
    sorted_lpositions = sorted(cleaned_lpositions.items(), key = lambda x: x[1])

    # sort the right positions by which has the smallest index
    sorted_rpositions = sorted(cleaned_rpositions.items(), key = lambda x: x[1])

    # compute value
    rval = int(str(sorted_lpositions[0][0]) + str(sorted_rpositions[-1][0]))

    DEBUG = not True
    if DEBUG:
        print("####")
        print(cpy_line, end="")
        print(line, end="")
        print(sorted_lpositions)
        print(sorted_rpositions)
        print(rval)
        print("####")

    return rval

def partTwo():
    with open("./input") as f:

        everything = []

        for line in f:
            temp = parseline(line)
            everything.append(temp)
    
        print(everything)
        print(sum(everything))

partOne()
partTwo()
