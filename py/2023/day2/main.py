def partOne():

    quantities = {
        "red" : "12",
        "green" : "13",
        "blue" : "14"
    }

    file = open("./input")
    # file = open("./smallinput")

    possible_ids = []

    for game in file:
        game_id = game.split(":")[0][5::]
        dirty_rounds = [_.split(",") for _ in game.split(":")[1].split(";")]
        clean_rounds = [[entry.strip().split(" ") for entry in _round] for _round in dirty_rounds]

        failed_check = False
        for _round in clean_rounds:
            round_data = {entry[1]: entry[0] for entry in _round}

            for color in round_data.keys():
                if int(round_data[color]) > int(quantities[color]):
                    failed_check = True
                    break

            if failed_check:
                break
            
        if not failed_check:
            possible_ids.append(int(game_id))

    print(sum(possible_ids))

def partTwo():

    file = open("./input")
    # file = open("./smallinput")

    powers = []

    for game in file:
        game_id = game.split(":")[0][5::]
        dirty_rounds = [_.split(",") for _ in game.split(":")[1].split(";")]
        clean_rounds = [[entry.strip().split(" ") for entry in _round] for _round in dirty_rounds]

        minimum_values = {
                "red" : 0,
                "green" : 0,
                "blue" : 0
        }

        for _round in clean_rounds:
            round_data = {entry[1]: entry[0] for entry in _round}
            
            for color in round_data.keys():
                if int(round_data[color]) >= minimum_values[color]:
                    minimum_values.update({color : int(round_data[color])})
        
        product = 1
        [product := product * value for value in minimum_values.values()]
        powers.append(product)

    print(sum(powers))

partOne()
partTwo()
