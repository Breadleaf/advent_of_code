from event import *
t1 = int
t2 = int

a_tests = []
def part_a(data):
    pile = [["C", "Z", "N", "B", "M", "W", "Q", "V"], # 0
            ["H", "Z", "R", "W", "C", "B"],           # 1
            ["F", "Q", "R", "J"],                     # 2
            ["Z", "S", "W", "H", "F", "N", "M", "T"], # 3
            ["G", "F", "W", "L", "N", "Q", "P"],      # 4
            ["L", "P", "W"],                          # 5
            ["V", "B", "D", "R", "G", "C", "Q", "J"], # 6
            ["Z", "Q", "N", "B", "W"],                # 7
            ["H", "L", "F", "C", "G", "T", "J"]]      # 8

    for instruction in data:
        #for _ in range(instruction[0]):
            #pile[instruction[2] - 1].append(pile[instruction[1] - 1].pop())
        pile[instruction[2] - 1] += reversed(pile[instruction[1] - 1][-instruction[0]:])
        pile[instruction[1] - 1] = pile[instruction[1] - 1][:-instruction[0]]

    msg = ""
    for p in pile:
        msg = f"{msg}{p[-1]}"

    return msg

run_b = 1
b_tests = []
def part_b(data):
    pile = [["C", "Z", "N", "B", "M", "W", "Q", "V"], # 0
            ["H", "Z", "R", "W", "C", "B"],           # 1
            ["F", "Q", "R", "J"],                     # 2
            ["Z", "S", "W", "H", "F", "N", "M", "T"], # 3
            ["G", "F", "W", "L", "N", "Q", "P"],      # 4
            ["L", "P", "W"],                          # 5
            ["V", "B", "D", "R", "G", "C", "Q", "J"], # 6
            ["Z", "Q", "N", "B", "W"],                # 7
            ["H", "L", "F", "C", "G", "T", "J"]]      # 8

    for instruction in data:
        pile[instruction[2] - 1] += pile[instruction[1] - 1][-instruction[0]:]
        pile[instruction[1] - 1] = pile[instruction[1] - 1][:-instruction[0]]

    msg = ""
    for p in pile:
        #msg = f"{msg}{p[-1]}"
        msg += p[-1]

    return msg

if __name__ == "__main__":
    # parse = lambda stream: [x.strip() for x in stream] # Line Breaks
    # parse = lambda stream: [x.strip() for x in stream if x.strip() != ""] # No Line Breaks
    # parse = lambda stream: [x.strip().split(" ") for x in stream] # Line Breaks Multiple Elements
    parse = lambda stream: [x.strip().split(" ") for x in stream if x.strip() != ""] # No Line Breaks Multiple Elements

    # convert = lambda lst: [t1(x) for x in lst] # Single Elements
    # convert = lambda lst: [[t1(x[0]), t2(x[1])] for x in lst] # Multiple Elements
    # convert = lambda lst: [[t1(y) for y in x] for x in lst] # Split Line Multiple Line
    # convert = lambda lst: [[t1(y) for y in x] for x in lst][0] # Split Line Multiple Line

    convert = lambda lst: [[int(x[1]), int(x[3]), int(x[5])] for x in lst]

    if run_tests("A", part_a, parse, convert, a_tests):
        print(part_a(convert(parse(open("input")))))

    if not run_b:
        exit(0)

    print()

    if run_tests("B", part_b, parse, convert, b_tests):
        print(part_b(convert(parse(open("input")))))
