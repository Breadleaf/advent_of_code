from event import *
t1 = str
t2 = str
t3 = float

data = [x.strip().split(" ") for x in open("./input")] # Line Breaks Non Uniform Data

data = [[t2(x[0]), t1(x[1])] for x in data]

total_score = 0

score_convert = {
        "A": 1,
        "X": 1, # rock
        "B": 2,
        "Y": 2, # paper
        "C": 3,
        "Z": 3  # scissors
        }

def outcome(o, y):
    if o == "A" and y == "X":
        return 3
    if o == "B" and y == "Y":
        return 3
    if o == "C" and y == "Z":
        return 3
    if o == "A" and y == "Z":
        return 0
    if o == "B" and y == "X":
        return 0
    if o == "C" and y == "Y":
        return 0
    return 6

for opp_play, you_play in data:
    total_score += score_convert[you_play] + outcome(opp_play, you_play)

print(total_score)

# Part 2

round_score = {
        "Z": 6,
        "Y": 3,
        "X": 0
        }

total_score = 0

for opp_play, arranged_outcome in data:

    you_play = ""

    if arranged_outcome == "Z":
        if opp_play == "A":
            you_play = "Y"
        if opp_play == "B":
            you_play = "Z"
        if opp_play == "C":
            you_play = "X"
    if arranged_outcome == "Y":
        if opp_play == "A":
            you_play = "X"
        if opp_play == "B":
            you_play = "Y"
        if opp_play == "C":
            you_play = "Z"
    if arranged_outcome == "X":
        if opp_play == "A":
            you_play = "Z"
        if opp_play == "B":
            you_play = "X"
        if opp_play == "C":
            you_play = "Y"

    total_score += score_convert[you_play] + round_score[arranged_outcome]

print(total_score)
