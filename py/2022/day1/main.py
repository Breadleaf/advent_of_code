def main():
    cals = [x.strip() for x in open("./input").readlines()]
    elfs = []

    temp = []
    for el in cals:
        if len(el) == 0:
            elfs.append(temp)
            temp = []
        else:
            temp.append(int(el))

    elf_total = []
    for el in elfs:
        elf_total.append(sum(el))

    print(max(elf_total))
    print(sum(sorted(elf_total)[::-1][:3]))

if __name__ == "__main__":
    main()
