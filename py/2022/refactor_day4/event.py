import random
import string
import enum
import time
import math
import json
import csv
import io

def test_eq(value_1, value_2):
    if value_1 == value_2:
        print("\033[0;32m[ PASSED ]\033[0;0m")
        return True
    else:
        print(f"\033[0;31m[ FAILED ]\033[0;0m\nIn: {value_1}\nEx: {value_2}")
        return False

def run_tests(part, func, parse, convert, tests):
    print("\033[0;32m[========]\033[0;0m")
    print(f"\033[0;32m[ RUN  {part} ]\033[0;0m")

    failed = 0
    for test in tests:
        print("\033[0;32m[  TEST  ]\033[0;0m")
        if not test_eq(func(convert(parse(io.StringIO(test[0])))), test[1]): failed += 1

    print("\033[0;32m[========]\033[0;0m")
    print(f"{failed} FAILED TESTS")

    return failed == 0
