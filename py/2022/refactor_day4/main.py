from event import *
t1 = str
t2 = int

a_tests = [["2-4,6-8", 0], ["2-3,4-5", 0], ["5-7,7-9", 0],
           ["2-8,3-7", 1], ["6-6,4-6", 1], ["2-6,4-8", 0],
           ["35-73,35-82", 1], ["17-17,17-84", 1], ["4-4,4-4", 1]]
def part_a(data):
    total_contained_pairs = 0

    for elf1, elf2 in data:
        elf1 = elf1.split("-")
        elf2 = elf2.split("-")

        if int(elf1[0]) <= int(elf2[0]) and int(elf1[1]) >= int(elf2[1]) or \
           int(elf2[0]) <= int(elf1[0]) and int(elf2[1]) >= int(elf1[1]):
            total_contained_pairs += 1

    return total_contained_pairs

run_b = 1
b_tests = [["2-4,6-8\n2-3,4-5\n5-7,7-9\n2-8,3-7\n6-6,4-6\n2-6,4-8", 4]]
def part_b(data):
    total_contained_pairs = 0

    for elf1, elf2 in data:
        a, b = elf1.split("-")
        elf1 = [x for x in range(int(a), int(b) + 1)]
        a, b = elf2.split("-")
        elf2 = [x for x in range(int(a), int(b) + 1)]

        for section in elf1:
            if section in elf2:
                total_contained_pairs += 1
                break

    return total_contained_pairs

if __name__ == "__main__":
    # parse = lambda stream: [x.strip() for x in stream] # Line Breaks
    # parse = lambda stream: [x.strip() for x in stream if x.strip() != ""] # No Line Breaks
    # parse = lambda stream: [x.strip().split(" ") for x in stream] # Line Breaks Multiple Elements
    # parse = lambda stream: [x.strip().split(" ") for x in stream if x.strip() != ""] # No Line Breaks Multiple Elements

    parse = lambda stream: [x for x in csv.reader(stream)]

    # convert = lambda lst: [t1(x) for x in lst] # Single Elements
    # convert = lambda lst: [[t1(x[0]), t2(x[1])] for x in lst] # Multiple Elements
    # convert = lambda lst: [[t1(y) for y in x] for x in lst] # Split Line Multiple Line
    # convert = lambda lst: [[t1(y) for y in x] for x in lst][0] # Split Line Multiple Line

    # convert = lambda lst: [x.split(",") for x in lst]
    convert = lambda lst: lst

    if run_tests("A", part_a, parse, convert, a_tests):
        print(part_a(convert(parse(open("input")))))

    if not run_b:
        exit(0)

    print()

    if run_tests("B", part_b, parse, convert, b_tests):
        print(part_b(convert(parse(open("input")))))
