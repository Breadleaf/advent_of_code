from event import *
t1 = str
t2 = str

def priority(letter):
    if letter in string.ascii_lowercase:
        return ord(letter) - 96
    else:
        return ord(letter) - 38

a_tests = [["vJrwpWtwJgWrhcsFMMfFFhFp\njqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL\nPmmdzqPrVvPwwTWBwg\nwMqvLMZHhHMvwLHjbvcjnnSBnvTQFn\nttgJtRGJQctTZtZT\nCrZsJsPPZsGzwwsLwLmpwMDw", 157], ["PmmdzqPrVvPwwTWBwg", 42]]
def part_a(data):
    total = 0
    for compartment_one, compartment_two in data:
        compartment_one = set(compartment_one)
        compartment_two = set(compartment_two)

        for letter in compartment_one:
            if letter in compartment_two:
                total += priority(letter)
    return total

run_b = 1
b_tests = [["vJrwpWtwJgWrhcsFMMfFFhFp\njqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL\nPmmdzqPrVvPwwTWBwg\nwMqvLMZHhHMvwLHjbvcjnnSBnvTQFn\nttgJtRGJQctTZtZT\nCrZsJsPPZsGzwwsLwLmpwMDw", 70]]
def part_b(data):
    total = 0
    for idx in range(0, len(data), 3):
        common = ""
        for letter in data[idx]:
            if letter in data[idx + 1] and letter in data[idx + 2]:
                common = letter
                break
        total += priority(common)
    return total

if __name__ == "__main__":
    # parse = lambda stream: [x.strip() for x in stream] # Line Breaks
    parse = lambda stream: [x.strip() for x in stream if x.strip() != ""] # No Line Breaks
    # parse = lambda stream: [x.strip().split(" ") for x in stream] # Line Breaks Multiple Elements
    # parse = lambda stream: [x.strip().split(" ") for x in stream if x.strip() != ""] # No Line Breaks Multiple Elements

    # convert = lambda lst: [t1(x) for x in lst] # Single Elements
    # convert = lambda lst: [[t1(x[0]), t2(x[1])] for x in lst] # Multiple Elements
    # convert = lambda lst: [[t1(y) for y in x] for x in lst] # Split Line Multiple Line
    # convert = lambda lst: [[t1(y) for y in x] for x in lst][0] # Split Line Multiple Line

    convert = lambda lst: [[x[:len(x)//2], x[len(x)//2:]] for x in lst]

    if run_tests("A", part_a, parse, convert, a_tests):
        print(part_a(convert(parse(open("input")))))

    if not run_b:
        exit(0)

    print()

    convert = lambda lst: [x for x in lst]

    if run_tests("B", part_b, parse, convert, b_tests):
        print(part_b(convert(parse(open("input")))))
