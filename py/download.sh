#!/bin/bash

begin () {
	open_page=true

	cookie=$(cat ../cookie)
	day=$(TZ="America/New_York" date +%-d)
	year=$(TZ="America/New_York" date +%Y)

	mkdir -p "${year}/day${day}"
	cp -n ./starter_code/* "${year}/day${day}"

	curl -s --cookie "session=${cookie}" "https://adventofcode.com/${year}/day/${day}/input" > "${year}/day${day}/input"

	if $open_page
	then
		xdg-open "https://adventofcode.com/${year}/day/${day}"
	fi

	ret="cd ${year}/day${day}" # && code ${year}/day${day}"
}

$1

echo $ret
