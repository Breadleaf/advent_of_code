from event import *
t1 = str
t2 = int

a_tests = []
def part_a(data):
    floor = 0
    for instruction in data:
        if instruction == "(":
            floor += 1
        else:
            floor -= 1
    return floor

run_b = 1
b_tests = [[")", 1], ["()())", 5]]
def part_b(data):
    floor = 0
    for idx, el in enumerate(data):
        if el == "(":
            floor += 1
        else:
            floor -= 1
        if floor == -1:
            return idx + 1

if __name__ == "__main__":
    # parse = lambda stream: [x.strip() for x in stream] # Line Breaks
    parse = lambda stream: [x.strip() for x in stream if x.strip() != ""] # No Line Breaks
    # parse = lambda stream: [x.strip().split(" ") for x in stream] # Line Breaks Multiple Elements
    # parse = lambda stream: [x.strip().split(" ") for x in stream if x.strip() != ""] # No Line Breaks Multiple Elements

    # convert = lambda lst: [t1(x) for x in lst] # Single Elements
    # convert = lambda lst: [[t1(x[0]), t2(x[1])] for x in lst] # Multiple Elements
    # convert = lambda lst: [[t1(y) for y in x] for x in lst] # Split Line Multiple Line
    convert = lambda lst: [[t1(y) for y in x] for x in lst][0] # Split Line Multiple Line

    if run_tests("A", part_a, parse, convert, a_tests):
        print(part_a(convert(parse(open("input")))))

    if not run_b:
        exit(0)

    print()

    if run_tests("B", part_b, parse, convert, b_tests):
        print(part_b(convert(parse(open("input")))))
