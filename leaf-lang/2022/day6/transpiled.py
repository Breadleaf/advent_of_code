import sys
stack = []
variables = {}
stack.append(1.0)
stack.append(1.0)
a = stack.pop()
b = stack.pop()
c = True if a == b else False
stack.append(b)
stack.append(a)
stack.append(c)
variables.update({"true" : stack.pop()})
stack.pop()
stack.pop()
stack.append(1.0)
stack.append(2.0)
a = stack.pop()
b = stack.pop()
c = True if a == b else False
stack.append(b)
stack.append(a)
stack.append(c)
variables.update({"false" : stack.pop()})
stack.pop()
stack.pop()
stack.append("./input")
variables.update({"fileName" : stack.pop()})
stack.append(variables["fileName"])
stack.append(open(str(stack.pop())).read()[:-1])
variables.update({"fileContents" : stack.pop()})
# Called Macro
stack.append(variables["fileContents"])
# Called Macro
stack.append("")
variables.update({"reverseTmpString" : stack.pop()})
variables.update({"reverseInput" : stack.pop()})
stack.append(0.0)
stack.append(variables["reverseInput"])
stack += [c for c in stack.pop()]
stack.append(0.0)
a = stack.pop()
b = stack.pop()
c = True if a == b else False
stack.append(b)
stack.append(a)
stack.append(c)
# Called Macro
stack.append(variables["true"])
variables.update({"notTmpReturn" : stack.pop()})
if stack[-1] == True:
	stack.append(variables["false"])
	variables.update({"notTmpReturn" : stack.pop()})
stack.pop()
stack.append(variables["notTmpReturn"])

while stack[-1] == True:
	stack.pop()
	stack.pop()
	variables.update({"reverseTmpChar" : stack.pop()})
	stack.append(variables["reverseTmpString"])
	stack.append(variables["reverseTmpChar"])
	a = stack.pop()
	b = stack.pop()
	a_num = False
	b_num = False
	if isinstance(a, float) or isinstance(a, int):
		a_num = True
	if isinstance(b, float) or isinstance(b, int):
		b_num = True
	if (a_num and b_num) or (not a_num and not b_num):
		stack.append(b + a)
	else:
		stack.append(str(b) + str(a))
	variables.update({"reverseTmpString" : stack.pop()})
	stack.append(0.0)
	a = stack.pop()
	b = stack.pop()
	c = True if a == b else False
	stack.append(b)
	stack.append(a)
	stack.append(c)
	# Called Macro
	stack.append(variables["true"])
	variables.update({"notTmpReturn" : stack.pop()})
	if stack[-1] == True:
		stack.append(variables["false"])
		variables.update({"notTmpReturn" : stack.pop()})
	stack.pop()
	stack.append(variables["notTmpReturn"])
stack.pop()
stack.pop()
stack.append(variables["reverseTmpString"])

stack += [c for c in stack.pop()]

stack.append(4.0)
variables.update({"index" : stack.pop()})
stack.append(variables["true"])
variables.update({"loopRunning" : stack.pop()})
stack.append(variables["loopRunning"])
while stack[-1] == True:
	stack.pop()
	variables.update({"bufferOne" : stack.pop()})
	variables.update({"bufferTwo" : stack.pop()})
	variables.update({"bufferThree" : stack.pop()})
	variables.update({"bufferFour" : stack.pop()})
	stack.append(variables["bufferFour"])
	stack.append(variables["bufferThree"])
	stack.append(variables["bufferTwo"])
	stack.append(variables["true"])
	variables.update({"allUnique" : stack.pop()})
	stack.append(variables["bufferOne"])
	stack.append(variables["bufferTwo"])
	a = stack.pop()
	b = stack.pop()
	c = True if a == b else False
	stack.append(b)
	stack.append(a)
	stack.append(c)
	if stack[-1] == True:
		stack.append(variables["false"])
		variables.update({"allUnique" : stack.pop()})
	stack.pop()
	stack.pop()
	stack.pop()
	stack.append(variables["bufferOne"])
	stack.append(variables["bufferThree"])
	a = stack.pop()
	b = stack.pop()
	c = True if a == b else False
	stack.append(b)
	stack.append(a)
	stack.append(c)
	if stack[-1] == True:
		stack.append(variables["false"])
		variables.update({"allUnique" : stack.pop()})
	stack.pop()
	stack.pop()
	stack.pop()
	stack.append(variables["bufferOne"])
	stack.append(variables["bufferFour"])
	a = stack.pop()
	b = stack.pop()
	c = True if a == b else False
	stack.append(b)
	stack.append(a)
	stack.append(c)
	if stack[-1] == True:
		stack.append(variables["false"])
		variables.update({"allUnique" : stack.pop()})
	stack.pop()
	stack.pop()
	stack.pop()
	stack.append(variables["bufferTwo"])
	stack.append(variables["bufferThree"])
	a = stack.pop()
	b = stack.pop()
	c = True if a == b else False
	stack.append(b)
	stack.append(a)
	stack.append(c)
	if stack[-1] == True:
		stack.append(variables["false"])
		variables.update({"allUnique" : stack.pop()})
	stack.pop()
	stack.pop()
	stack.pop()
	stack.append(variables["bufferTwo"])
	stack.append(variables["bufferFour"])
	a = stack.pop()
	b = stack.pop()
	c = True if a == b else False
	stack.append(b)
	stack.append(a)
	stack.append(c)
	if stack[-1] == True:
		stack.append(variables["false"])
		variables.update({"allUnique" : stack.pop()})
	stack.pop()
	stack.pop()
	stack.pop()
	stack.append(variables["bufferThree"])
	stack.append(variables["bufferFour"])
	a = stack.pop()
	b = stack.pop()
	c = True if a == b else False
	stack.append(b)
	stack.append(a)
	stack.append(c)
	if stack[-1] == True:
		stack.append(variables["false"])
		variables.update({"allUnique" : stack.pop()})
	stack.pop()
	stack.pop()
	stack.pop()
	stack.append(0.0)
	a = stack.pop()
	b = stack.pop()
	c = True if a == b else False
	stack.append(b)
	stack.append(a)
	stack.append(c)
	# Called Macro
	stack.append(variables["true"])
	variables.update({"notTmpReturn" : stack.pop()})
	if stack[-1] == True:
		stack.append(variables["false"])
		variables.update({"notTmpReturn" : stack.pop()})
	stack.pop()
	stack.append(variables["notTmpReturn"])
	variables.update({"loopRunning" : stack.pop()})
	stack.pop()
	stack.append(variables["allUnique"])
	stack.append(variables["true"])
	a = stack.pop()
	b = stack.pop()
	c = True if a == b else False
	stack.append(b)
	stack.append(a)
	stack.append(c)
	if stack[-1] == True:
		stack.append(variables["index"])
		print(stack.pop(), end="")
		stack.append("\n")
		print(stack.pop(), end="")
		stack.append(variables["false"])
		variables.update({"loopRunning" : stack.pop()})
	stack.pop()
	stack.pop()
	stack.pop()
	stack.append(variables["loopRunning"])
	stack.append(variables["index"])
	stack.append(1.0)
	a = stack.pop()
	b = stack.pop()
	a_num = False
	b_num = False
	if isinstance(a, float) or isinstance(a, int):
		a_num = True
	if isinstance(b, float) or isinstance(b, int):
		b_num = True
	if (a_num and b_num) or (not a_num and not b_num):
		stack.append(b + a)
	else:
		stack.append(str(b) + str(a))
	variables.update({"index" : stack.pop()})
stack.pop()
stack.pop()
stack.append(0.0)
a = stack.pop()
b = stack.pop()
c = True if a == b else False
stack.append(b)
stack.append(a)
stack.append(c)
# Called Macro
stack.append(variables["true"])
variables.update({"notTmpReturn" : stack.pop()})
if stack[-1] == True:
	stack.append(variables["false"])
	variables.update({"notTmpReturn" : stack.pop()})
stack.pop()
stack.append(variables["notTmpReturn"])

while stack[-1] == True:
	stack.pop()
	stack.pop()
	stack.pop()
	stack.append(0.0)
	a = stack.pop()
	b = stack.pop()
	c = True if a == b else False
	stack.append(b)
	stack.append(a)
	stack.append(c)
	# Called Macro
	stack.append(variables["true"])
	variables.update({"notTmpReturn" : stack.pop()})
	if stack[-1] == True:
		stack.append(variables["false"])
		variables.update({"notTmpReturn" : stack.pop()})
	stack.pop()
	stack.append(variables["notTmpReturn"])
stack.pop()
stack.pop()
stack.pop()
# Called Macro
stack.append(variables["fileContents"])
# Called Macro
stack.append("")
variables.update({"reverseTmpString" : stack.pop()})
variables.update({"reverseInput" : stack.pop()})
stack.append(0.0)
stack.append(variables["reverseInput"])
stack += [c for c in stack.pop()]
stack.append(0.0)
a = stack.pop()
b = stack.pop()
c = True if a == b else False
stack.append(b)
stack.append(a)
stack.append(c)
# Called Macro
stack.append(variables["true"])
variables.update({"notTmpReturn" : stack.pop()})
if stack[-1] == True:
	stack.append(variables["false"])
	variables.update({"notTmpReturn" : stack.pop()})
stack.pop()
stack.append(variables["notTmpReturn"])

while stack[-1] == True:
	stack.pop()
	stack.pop()
	variables.update({"reverseTmpChar" : stack.pop()})
	stack.append(variables["reverseTmpString"])
	stack.append(variables["reverseTmpChar"])
	a = stack.pop()
	b = stack.pop()
	a_num = False
	b_num = False
	if isinstance(a, float) or isinstance(a, int):
		a_num = True
	if isinstance(b, float) or isinstance(b, int):
		b_num = True
	if (a_num and b_num) or (not a_num and not b_num):
		stack.append(b + a)
	else:
		stack.append(str(b) + str(a))
	variables.update({"reverseTmpString" : stack.pop()})
	stack.append(0.0)
	a = stack.pop()
	b = stack.pop()
	c = True if a == b else False
	stack.append(b)
	stack.append(a)
	stack.append(c)
	# Called Macro
	stack.append(variables["true"])
	variables.update({"notTmpReturn" : stack.pop()})
	if stack[-1] == True:
		stack.append(variables["false"])
		variables.update({"notTmpReturn" : stack.pop()})
	stack.pop()
	stack.append(variables["notTmpReturn"])
stack.pop()
stack.pop()
stack.append(variables["reverseTmpString"])

stack += [c for c in stack.pop()]

stack.append(14.0)
variables.update({"index" : stack.pop()})
stack.append("Begin to compute part 2 (this will take a little bit of time)\n")
print(stack.pop(), end="")
stack.append(variables["true"])
variables.update({"loopRunning" : stack.pop()})
stack.append(variables["loopRunning"])
while stack[-1] == True:
	stack.pop()
	stack.append(14.0)
	variables.update({"charsLeft" : stack.pop()})
	stack.append("")
	variables.update({"buffer" : stack.pop()})
	stack.append(variables["charsLeft"])
	stack.append(0.0)
	a = stack.pop()
	b = stack.pop()
	c = True if a == b else False
	stack.append(b)
	stack.append(a)
	stack.append(c)
	# Called Macro
	stack.append(variables["true"])
	variables.update({"notTmpReturn" : stack.pop()})
	if stack[-1] == True:
		stack.append(variables["false"])
		variables.update({"notTmpReturn" : stack.pop()})
	stack.pop()
	stack.append(variables["notTmpReturn"])
	while stack[-1] == True:
		stack.pop()
		stack.pop()
		stack.pop()
		stack.append(variables["buffer"])
		a = stack.pop()
		b = stack.pop()
		a_num = False
		b_num = False
		if isinstance(a, float) or isinstance(a, int):
			a_num = True
		if isinstance(b, float) or isinstance(b, int):
			b_num = True
		if (a_num and b_num) or (not a_num and not b_num):
			stack.append(b + a)
		else:
			stack.append(str(b) + str(a))
		variables.update({"buffer" : stack.pop()})
		stack.append(variables["charsLeft"])
		stack.append(1.0)
		a = b = None
		try:
			a = float(stack.pop())
			b = float(stack.pop())
		except ValueError:
			print("Error: Top two values of stack must be of type number to use math operator")
			exit(1)
		stack.append(b - a)
		variables.update({"charsLeft" : stack.pop()})
		stack.append(variables["charsLeft"])
		stack.append(0.0)
		a = stack.pop()
		b = stack.pop()
		c = True if a == b else False
		stack.append(b)
		stack.append(a)
		stack.append(c)
		# Called Macro
		stack.append(variables["true"])
		variables.update({"notTmpReturn" : stack.pop()})
		if stack[-1] == True:
			stack.append(variables["false"])
			variables.update({"notTmpReturn" : stack.pop()})
		stack.pop()
		stack.append(variables["notTmpReturn"])
	stack.pop()
	stack.pop()
	stack.pop()
	stack.append(variables["buffer"])
	variables.update({"bufferCopy" : stack.pop()})
	stack.append(variables["true"])
	variables.update({"allUnique" : stack.pop()})
	stack.append(variables["true"])
	variables.update({"looping" : stack.pop()})
	stack.append(variables["looping"])
	while stack[-1] == True:
		stack.pop()
		stack.append(0.0)
		stack.append(variables["bufferCopy"])
		stack += [c for c in stack.pop()]
		variables.update({"current" : stack.pop()})
		# Called Macro
		stack.append("")
		variables.update({"formatTmpOutput" : stack.pop()})
		stack.append(0.0)
		a = stack.pop()
		b = stack.pop()
		c = True if a == b else False
		stack.append(b)
		stack.append(a)
		stack.append(c)
		# Called Macro
		stack.append(variables["true"])
		variables.update({"notTmpReturn" : stack.pop()})
		if stack[-1] == True:
			stack.append(variables["false"])
			variables.update({"notTmpReturn" : stack.pop()})
		stack.pop()
		stack.append(variables["notTmpReturn"])
		while stack[-1] == True:
			stack.pop()
			stack.pop()
			stack.append(str(stack.pop()))
			variables.update({"formatTmpInput" : stack.pop()})
			stack.append(variables["formatTmpOutput"])
			stack.append(variables["formatTmpInput"])
			a = stack.pop()
			b = stack.pop()
			a_num = False
			b_num = False
			if isinstance(a, float) or isinstance(a, int):
				a_num = True
			if isinstance(b, float) or isinstance(b, int):
				b_num = True
			if (a_num and b_num) or (not a_num and not b_num):
				stack.append(b + a)
			else:
				stack.append(str(b) + str(a))
			variables.update({"formatTmpOutput" : stack.pop()})
			stack.append(0.0)
			a = stack.pop()
			b = stack.pop()
			c = True if a == b else False
			stack.append(b)
			stack.append(a)
			stack.append(c)
			# Called Macro
			stack.append(variables["true"])
			variables.update({"notTmpReturn" : stack.pop()})
			if stack[-1] == True:
				stack.append(variables["false"])
				variables.update({"notTmpReturn" : stack.pop()})
			stack.pop()
			stack.append(variables["notTmpReturn"])
		stack.pop()
		stack.pop()
		stack.pop()
		stack.append(variables["formatTmpOutput"])
		variables.update({"bufferCopy" : stack.pop()})
		stack.append(variables["bufferCopy"])
		stack.append(variables["current"])
		# Called Macro
		variables.update({"EVAL" : stack.pop()})
		variables.update({"STR" : stack.pop()})
		stack.append(variables["false"])
		variables.update({"inSTR" : stack.pop()})
		stack.append(0.0)
		stack.append(variables["STR"])
		stack += [c for c in stack.pop()]
		stack.append(0.0)
		a = stack.pop()
		b = stack.pop()
		c = True if a == b else False
		stack.append(b)
		stack.append(a)
		stack.append(c)
		# Called Macro
		stack.append(variables["true"])
		variables.update({"notTmpReturn" : stack.pop()})
		if stack[-1] == True:
			stack.append(variables["false"])
			variables.update({"notTmpReturn" : stack.pop()})
		stack.pop()
		stack.append(variables["notTmpReturn"])
		while stack[-1] == True:
			stack.pop()
			stack.pop()
			stack.append(variables["EVAL"])
			a = stack.pop()
			b = stack.pop()
			c = True if a == b else False
			stack.append(b)
			stack.append(a)
			stack.append(c)
			if stack[-1] == True:
				stack.append(variables["true"])
				variables.update({"inSTR" : stack.pop()})
			stack.pop()
			stack.pop()
			stack.pop()
			stack.append(0.0)
			a = stack.pop()
			b = stack.pop()
			c = True if a == b else False
			stack.append(b)
			stack.append(a)
			stack.append(c)
			# Called Macro
			stack.append(variables["true"])
			variables.update({"notTmpReturn" : stack.pop()})
			if stack[-1] == True:
				stack.append(variables["false"])
				variables.update({"notTmpReturn" : stack.pop()})
			stack.pop()
			stack.append(variables["notTmpReturn"])
		stack.pop()
		stack.pop()
		stack.pop()
		stack.append(variables["inSTR"])
		if stack[-1] == True:
			stack.append(variables["false"])
			variables.update({"allUnique" : stack.pop()})
		stack.pop()
		stack.append(variables["bufferCopy"])
		# Called Macro
		stack.append(0.0)
		variables.update({"LEN" : stack.pop()})
		variables.update({"STR" : stack.pop()})
		stack.append(0.0)
		stack.append(variables["STR"])
		stack += [c for c in stack.pop()]
		stack.append(0.0)
		a = stack.pop()
		b = stack.pop()
		c = True if a == b else False
		stack.append(b)
		stack.append(a)
		stack.append(c)
		# Called Macro
		stack.append(variables["true"])
		variables.update({"notTmpReturn" : stack.pop()})
		if stack[-1] == True:
			stack.append(variables["false"])
			variables.update({"notTmpReturn" : stack.pop()})
		stack.pop()
		stack.append(variables["notTmpReturn"])
		while stack[-1] == True:
			stack.pop()
			stack.pop()
			stack.append("")
			a = stack.pop()
			b = stack.pop()
			c = True if a == b else False
			stack.append(b)
			stack.append(a)
			stack.append(c)
			# Called Macro
			stack.append(variables["true"])
			variables.update({"notTmpReturn" : stack.pop()})
			if stack[-1] == True:
				stack.append(variables["false"])
				variables.update({"notTmpReturn" : stack.pop()})
			stack.pop()
			stack.append(variables["notTmpReturn"])
			if stack[-1] == True:
				stack.append(variables["LEN"])
				stack.append(1.0)
				a = stack.pop()
				b = stack.pop()
				a_num = False
				b_num = False
				if isinstance(a, float) or isinstance(a, int):
					a_num = True
				if isinstance(b, float) or isinstance(b, int):
					b_num = True
				if (a_num and b_num) or (not a_num and not b_num):
					stack.append(b + a)
				else:
					stack.append(str(b) + str(a))
				variables.update({"LEN" : stack.pop()})
			stack.pop()
			stack.pop()
			stack.pop()
			stack.append(0.0)
			a = stack.pop()
			b = stack.pop()
			c = True if a == b else False
			stack.append(b)
			stack.append(a)
			stack.append(c)
			# Called Macro
			stack.append(variables["true"])
			variables.update({"notTmpReturn" : stack.pop()})
			if stack[-1] == True:
				stack.append(variables["false"])
				variables.update({"notTmpReturn" : stack.pop()})
			stack.pop()
			stack.append(variables["notTmpReturn"])
		stack.pop()
		stack.pop()
		stack.pop()
		stack.append(variables["LEN"])
		stack.append(0.0)
		a = stack.pop()
		b = stack.pop()
		c = True if a == b else False
		stack.append(b)
		stack.append(a)
		stack.append(c)
		# Called Macro
		stack.append(variables["true"])
		variables.update({"notTmpReturn" : stack.pop()})
		if stack[-1] == True:
			stack.append(variables["false"])
			variables.update({"notTmpReturn" : stack.pop()})
		stack.pop()
		stack.append(variables["notTmpReturn"])
		variables.update({"looping" : stack.pop()})
		stack.pop()
		stack.pop()
		stack.append(variables["looping"])
	stack.pop()
	stack.append(variables["buffer"])
	stack += [c for c in stack.pop()]
	variables.update({"garbage" : stack.pop()})
	# Called Macro
	stack.append("")
	variables.update({"formatTmpOutput" : stack.pop()})
	stack.append(0.0)
	a = stack.pop()
	b = stack.pop()
	c = True if a == b else False
	stack.append(b)
	stack.append(a)
	stack.append(c)
	# Called Macro
	stack.append(variables["true"])
	variables.update({"notTmpReturn" : stack.pop()})
	if stack[-1] == True:
		stack.append(variables["false"])
		variables.update({"notTmpReturn" : stack.pop()})
	stack.pop()
	stack.append(variables["notTmpReturn"])
	while stack[-1] == True:
		stack.pop()
		stack.pop()
		stack.append(str(stack.pop()))
		variables.update({"formatTmpInput" : stack.pop()})
		stack.append(variables["formatTmpOutput"])
		stack.append(variables["formatTmpInput"])
		a = stack.pop()
		b = stack.pop()
		a_num = False
		b_num = False
		if isinstance(a, float) or isinstance(a, int):
			a_num = True
		if isinstance(b, float) or isinstance(b, int):
			b_num = True
		if (a_num and b_num) or (not a_num and not b_num):
			stack.append(b + a)
		else:
			stack.append(str(b) + str(a))
		variables.update({"formatTmpOutput" : stack.pop()})
		stack.append(0.0)
		a = stack.pop()
		b = stack.pop()
		c = True if a == b else False
		stack.append(b)
		stack.append(a)
		stack.append(c)
		# Called Macro
		stack.append(variables["true"])
		variables.update({"notTmpReturn" : stack.pop()})
		if stack[-1] == True:
			stack.append(variables["false"])
			variables.update({"notTmpReturn" : stack.pop()})
		stack.pop()
		stack.append(variables["notTmpReturn"])
	stack.pop()
	stack.pop()
	stack.pop()
	stack.append(variables["formatTmpOutput"])
	# Called Macro
	stack.append("")
	variables.update({"reverseTmpString" : stack.pop()})
	variables.update({"reverseInput" : stack.pop()})
	stack.append(0.0)
	stack.append(variables["reverseInput"])
	stack += [c for c in stack.pop()]
	stack.append(0.0)
	a = stack.pop()
	b = stack.pop()
	c = True if a == b else False
	stack.append(b)
	stack.append(a)
	stack.append(c)
	# Called Macro
	stack.append(variables["true"])
	variables.update({"notTmpReturn" : stack.pop()})
	if stack[-1] == True:
		stack.append(variables["false"])
		variables.update({"notTmpReturn" : stack.pop()})
	stack.pop()
	stack.append(variables["notTmpReturn"])
	while stack[-1] == True:
		stack.pop()
		stack.pop()
		variables.update({"reverseTmpChar" : stack.pop()})
		stack.append(variables["reverseTmpString"])
		stack.append(variables["reverseTmpChar"])
		a = stack.pop()
		b = stack.pop()
		a_num = False
		b_num = False
		if isinstance(a, float) or isinstance(a, int):
			a_num = True
		if isinstance(b, float) or isinstance(b, int):
			b_num = True
		if (a_num and b_num) or (not a_num and not b_num):
			stack.append(b + a)
		else:
			stack.append(str(b) + str(a))
		variables.update({"reverseTmpString" : stack.pop()})
		stack.append(0.0)
		a = stack.pop()
		b = stack.pop()
		c = True if a == b else False
		stack.append(b)
		stack.append(a)
		stack.append(c)
		# Called Macro
		stack.append(variables["true"])
		variables.update({"notTmpReturn" : stack.pop()})
		if stack[-1] == True:
			stack.append(variables["false"])
			variables.update({"notTmpReturn" : stack.pop()})
		stack.pop()
		stack.append(variables["notTmpReturn"])
	stack.pop()
	stack.pop()
	stack.append(variables["reverseTmpString"])
	stack += [c for c in stack.pop()]
	stack.append(variables["allUnique"])
	if stack[-1] == True:
		stack.append(variables["index"])
		print(stack.pop(), end="")
		stack.append("\n")
		print(stack.pop(), end="")
	# Called Macro
	stack.append(variables["true"])
	variables.update({"notTmpReturn" : stack.pop()})
	if stack[-1] == True:
		stack.append(variables["false"])
		variables.update({"notTmpReturn" : stack.pop()})
	stack.pop()
	stack.append(variables["notTmpReturn"])
	variables.update({"loopRunning" : stack.pop()})
	stack.append(variables["index"])
	stack.append(1.0)
	a = stack.pop()
	b = stack.pop()
	a_num = False
	b_num = False
	if isinstance(a, float) or isinstance(a, int):
		a_num = True
	if isinstance(b, float) or isinstance(b, int):
		b_num = True
	if (a_num and b_num) or (not a_num and not b_num):
		stack.append(b + a)
	else:
		stack.append(str(b) + str(a))
	variables.update({"index" : stack.pop()})
	stack.append(variables["loopRunning"])
stack.pop()