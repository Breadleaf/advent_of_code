# Setup:

- go to [Advent of Code](https://adventofcode.com)
- sign in with an account
- go to any year and any day
- open inspect element
- go to network tab
- click link to get your puzzle input
- find your GET request
- find cookie
- copy cookie (excluding "session=")
- paste into file named "cookie" in the same directory as this file
- type `eval $(./download.sh begin)`
